<?php

if( 'DEV' == ENVIRONNEMENT ){
  ini_set("display_errors", 1);
	error_reporting(E_ALL);
	// set debug mode
	$app['debug'] = true;
}


/**
 * Enregistrement de Twig
 */
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../templates',
    'cache' => false
));

/**
 * Enregistrement du provider de SESSION
 */
$app->register(new Silex\Provider\SessionServiceProvider());

/**
 * Validator : https://silex.sensiolabs.org/doc/2.0/providers/validator.html
 */
$app->register(new Silex\Provider\ValidatorServiceProvider());

/**
 * Doctrine
 */
include __DIR__.'/../config/config.php';
$app->register(new Silex\Provider\DoctrineServiceProvider, array(
    'dbs.options' => array (
    	 'main' => $dbConfig,
    ),	 
));

// Appel de la liste des types enum à ajouter à Doctrine
$allEnums = new \YannFourgaut\EnumDoctrine\Doctrine\ListEnums;

// https://github.com/dflydev/dflydev-doctrine-orm-service-provider
$app->register(new Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider, [
  'orm.proxies_dir'             => 'src/Entity/Proxy',
  'orm.auto_generate_proxies'   => $app['debug'],
  'orm.em.options'              => [
  	'connection' => 'main',
    'mappings' => [
      [
        'type'                         => 'annotation',
        'namespace'                    => 'src\\Entity\\',
        'path'                         => 'src/Entity',
        'use_simple_annotation_reader' => false,
      ],
      [
        'type'                         => 'annotation',
        'namespace'                    => 'src\\Repository\\',
        'path'                         => 'src/Repository',
        'use_simple_annotation_reader' => false,
      ],
    ],
    'types' => 
      	$allEnums->files
    ,
  ]
]);

$app->register(new Silex\Provider\SwiftmailerServiceProvider());
$app['swiftmailer.options'] = array(
    'host' => 'localhost',
    'port' => '2525'
);

$em = $app['orm.em'];
$conn = $em->getConnection();
$conn->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

// Ecriture de $app dans la variable static pour accessibilité
\src\Helpers\AppGlobal::$params = $app;
