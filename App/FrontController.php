<?php

namespace App;

/**
 * Appel par namespace de la class selon le nom de la route et le params action.
 *
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class FrontController{

	/**
     * @var boolean $twigCalled Un template twig est il appelé ?
     */
    public $twigCalled = 0;

    /**
     * @var string $template Le fichier de template TWIG
     */
    public $template;

    /**
     * @var array $params Les paramètres à transmettre au template TWIG
     */
    public $params;

    /**
     * @var string $json Le JSON à retourner
     */
    public $json;

    /**
     * @var string $redirect Le lien de redirection
     */
    public $redirect;

    /**
     * @var object $response L'objet response Symfony
     */
    public $response;

}
