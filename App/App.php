<?php

/**
 * Include de la connexion PDO
 */
include __DIR__.'/Define.php';
include __DIR__.'/Config.php';

use Symfony\Component\HttpFoundation\Request;

/**
 * @var object $request Récupération des request HTTP / cookies ...
 */
$request = new \Symfony\Component\HttpFoundation\Request;

/**
 * Initialisation du $_SESSION
 */
$app->before(function (Request $request) {
    $request->getSession()->start();
});


$app->mount('/', new App\Rooter());
