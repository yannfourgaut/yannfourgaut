**Conventions : **
	
	1 - Index.php : 
			Autoload / Instanciation Silex / include App.php

	2 - App/App.php : 
			Mount de la collection des controllers déclarés dans le Rooter et include de la config Silex et des define

	3 - App/Rooter : 
			Déclare la collection des controllers
			Définit le dossier possédant le fichier de la class à appeler, appelle la class de gestion d'erreurs si fichier non existant, prépare les parametres, les GET/POST HTTP et les cookies

	4 - src/Classes/Folder/MyClass : 
			Classe appelée par convention namespace CamelCase

	5 - App/Rooter : 
			Récupère et retourne les parametres de la class appelée
			Défini ce qui doit être affiché par ordre de priorité

	6 - App/App.php : 
			Retourne le retour d'affichage du rooter

	7 - Index.php :
			Run Silex

**Dépendances : **

	- SASS

**Add Ons : **
	
	- Node
	- Bower
	- Composer	

**Nota bene : **

	- Dossier vendor ignoré par git, se placer à la racine puis composer install.
	- Dossier bower_components ignoré par git, se placer à la racine puis bower install.
	- La compilation SASS est utilisée, merci d'appliquer les modifications css dans /web/scss en lançant la commande : 
		- sass --watch fichier.scss:../css/fichier.css