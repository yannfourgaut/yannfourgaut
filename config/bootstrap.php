<?php

require_once __DIR__."/../vendor/autoload.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver ; 
use Doctrine\Common\Annotations\AnnotationReader ; 
use Doctrine\DBAL\Types\Type;

$paths = array(realpath(__DIR__."/../src/Entity"), realpath(__DIR__."/../src/Repository"));
$isDevMode = true;

include __DIR__.'/config.php';
// the connection configuration
$dbParams = $dbConfig;

$driver = new AnnotationDriver(new AnnotationReader(),$paths);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$config->setMetadataDriverImpl($driver);

$entityManager = EntityManager::create($dbParams, $config);

// Adaptation des types de champs enum à Doctrine
$allEnums = new \YannFourgaut\EnumDoctrine\Doctrine\ListEnums;
foreach($allEnums->files as $class => $namespace){
	Type::addType( $class, $namespace);
}

$conn = $entityManager->getConnection();
$conn->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');