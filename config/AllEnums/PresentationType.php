<?php 

namespace config\AllEnums;

use \YannFourgaut\EnumDoctrine\Doctrine\EnumType as EnumType;

class PresentationType extends EnumType{

	protected $name = 'PresentationType';
    public $values = array('personnality', 'road', 'talent', 'about');

}