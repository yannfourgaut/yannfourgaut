<?php 

namespace src\Repository;

use Doctrine\ORM\EntityRepository;


class LangagesRepository extends EntityRepository
{
	public function returnAll()
    {
    	$app = \src\Helpers\AppGlobal::$params;

        return $app['orm.em']
            ->createQuery(
                "SELECT l.name,l.img FROM \src\Entity\Langages l ORDER BY l.name ASC"
            )
            ->getResult();
    }
}