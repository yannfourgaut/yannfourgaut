<?php 

namespace src\Repository;

use Doctrine\ORM\EntityRepository;


class UsersRepository extends EntityRepository
{
    public function Exists($pseudo,$pass)
    {
    	$app = \src\Helpers\AppGlobal::$params;

        return $app['orm.em']
            ->createQuery(
                "SELECT COUNT(u.id) as exist FROM \src\Entity\Users u WHERE u.pseudo = '{$pseudo}' AND u.pass = '{$pass}'"
            )
            ->getResult();
    }
}