<?php 

namespace src\Repository;

use Doctrine\ORM\EntityRepository;


class DescriptionRepository extends EntityRepository
{
    public function findByType($type)
    {
    	$app = \src\Helpers\AppGlobal::$params;

        return $app['orm.em']
            ->createQuery(
                "SELECT p.text FROM \src\Entity\Description p WHERE p.type = '{$type}' ORDER BY p.id ASC"
            )
            ->getResult();
    }

    public function findIdByType($type)
    {
        $app = \src\Helpers\AppGlobal::$params;

        return $app['orm.em']
            ->createQuery(
                "SELECT p.id FROM \src\Entity\Description p WHERE p.type = '{$type}' ORDER BY p.id ASC"
            )
            ->getResult();
    }
}