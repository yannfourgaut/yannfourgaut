<?php 

namespace src\Repository;

use Doctrine\ORM\EntityRepository;


class PackagesRepository extends EntityRepository
{
    public function findAll()
    {
    	$app = \src\Helpers\AppGlobal::$params;

        return $app['orm.em']
            ->createQuery(
                "SELECT p.name, p.description, p.link FROM \src\Entity\Packages p ORDER BY p.id ASC"
            )
            ->getResult();
    }
}