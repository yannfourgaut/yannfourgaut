<?php 

namespace src\Repository;

use Doctrine\ORM\EntityRepository;


class ExperiencesRepository extends EntityRepository
{
	public function returnInOrder()
    {
    	$app = \src\Helpers\AppGlobal::$params;
    	$experiences = array('actual' => '', 'passed' => '');

    	$experiences['actual'] = $app['orm.em']
            ->createQuery(
                "
                SELECT e.poste,e.enterprise,e.date_start,e.date_end,e.link,e.description,e.actual,e.id
                FROM \src\Entity\Experiences e 
                WHERE e.actual = 1
                ORDER BY e.date_end DESC
                "
            )
            ->getResult();

        $experiences['passed'] = $app['orm.em']
            ->createQuery(
                "
                SELECT e.poste,e.enterprise,e.date_start,e.date_end,e.link,e.description,e.actual,e.id
                FROM \src\Entity\Experiences e 
                WHERE e.actual = 0
                ORDER BY e.date_end DESC
                "
            )
            ->getResult();

        return $experiences;    
    }
}