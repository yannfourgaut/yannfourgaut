<?php 

namespace src\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="src\Repository\DescriptionRepository")
 * @ORM\Table(name="description")
 */

class Description
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="PresentationType")
     */
    private $type;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * Set id.
     *
     * @return int
     */
    public function setId($id)
    {

        $this->id = $id;
        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param PresentationType $type
     *
     * @return Description
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return PresentationType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set text.
     *
     * @param string $text
     *
     * @return Description
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}
