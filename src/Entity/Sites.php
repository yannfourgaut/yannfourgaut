<?php 

namespace src\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="sites")
 */

class Sites
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imageColor;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imageBlack;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Sites
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Sites
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return Sites
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set imageColor.
     *
     * @param string $imageColor
     *
     * @return Sites
     */
    public function setImageColor($imageColor)
    {
        $this->imageColor = $imageColor;

        return $this;
    }

    /**
     * Get imageColor.
     *
     * @return string
     */
    public function getImageColor()
    {
        return $this->imageColor;
    }

    /**
     * Set imageBlack.
     *
     * @param string $imageBlack
     *
     * @return Sites
     */
    public function setImageBlack($imageBlack)
    {
        $this->imageBlack = $imageBlack;

        return $this;
    }

    /**
     * Get imageBlack.
     *
     * @return string
     */
    public function getImageBlack()
    {
        return $this->imageBlack;
    }
}
