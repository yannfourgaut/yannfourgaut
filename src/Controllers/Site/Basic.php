<?php

namespace src\Controllers\Site;

use \App\FrontController as FrontController;
use \src\Helpers\AppGlobal as app;
use Symfony\Component\HttpFoundation as HttpFoundation;

/**
 * Class d'accès aux pages d'accueil
 *
 * Par convention, quand aucune action n'est appelée en query URL, c'est la class Basic qui est appelée
 * 
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Basic extends FrontController
{

  public function __construct( 
    array $params, 
    HttpFoundation\ServerBag $server, 
    HttpFoundation\FileBag $files, 
    HttpFoundation\ParameterBag $post, 
    HttpFoundation\ParameterBag $get, 
    HttpFoundation\ParameterBag $cookies
    )
  {
  		$app = app::$params;
  		$site = $app['orm.em']->getRepository('src\Entity\Sites')->find($params['id']);
  		$this->twigCalled = 1;
  		$this->template = 'sites.twig';
  		$this->params['site'] = $site;
  }
}  	