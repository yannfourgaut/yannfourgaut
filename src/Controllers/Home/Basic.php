<?php

namespace src\Controllers\Home;

use \App\FrontController as FrontController;
use Symfony\Component\HttpFoundation as HttpFoundation;

/**
 * Class d'accès aux pages d'accueil
 *
 * Par convention, quand aucune action n'est appelée en query URL, c'est la class Basic qui est appelée
 * 
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Basic extends FrontController
{

  public function __construct( 
    array $params, 
    HttpFoundation\ServerBag $server, 
    HttpFoundation\FileBag $files, 
    HttpFoundation\ParameterBag $post, 
    HttpFoundation\ParameterBag $get, 
    HttpFoundation\ParameterBag $cookies
    )
  {

      $this->template = 'home.twig';
      $this->twigCalled = 1;

      $description = \src\Helpers\Description::returnAll();  
      $packages = \src\Helpers\Packages::returnAll(); 
      $langages = \src\Helpers\Langages::returnAll();
      $experiences = \src\Helpers\Experiences::returnAllInOrder();

      $this->params['description'] = $description;    
      $this->params['packages'] = $packages; 
      $this->params['langages'] = $langages;
      $this->params['experiences'] = $experiences;
  }
}
