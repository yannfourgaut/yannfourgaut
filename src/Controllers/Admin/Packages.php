<?php

namespace src\Controllers\Admin;

use \App\FrontController as FrontController;
use \src\Helpers\AppGlobal as app;
use Symfony\Component\HttpFoundation as HttpFoundation;

/**
 * Class de retour des packages créés
 * 
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Packages extends FrontController
{

 public function __construct( 
    array $params, 
    HttpFoundation\ServerBag $server, 
    HttpFoundation\FileBag $files, 
    HttpFoundation\ParameterBag $post, 
    HttpFoundation\ParameterBag $get, 
    HttpFoundation\ParameterBag $cookies
    )
  {
    $this->twigCalled = 1;
    $this->template = 'admin/packages.twig';

    $app = app::$params;
    $this->params['packages'] = $app['orm.em']->getRepository('\src\Entity\Packages')->findAll();
  }
}
