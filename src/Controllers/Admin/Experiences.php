<?php

namespace src\Controllers\Admin;

use \App\FrontController as FrontController;

use Symfony\Component\HttpFoundation as HttpFoundation;
/**
 * Class de construction de la page accueil admin
 * 
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Experiences extends FrontController
{

  public function __construct( 
    array $params, 
    HttpFoundation\ServerBag $server, 
    HttpFoundation\FileBag $files, 
    HttpFoundation\ParameterBag $post, 
    HttpFoundation\ParameterBag $get, 
    HttpFoundation\ParameterBag $cookies
    )
  {
    
  	// Connecté
  	if( \src\Helpers\Site\Session::GetSession(SESSION_USER_NAME) !== 0 ){

      $days = array();
      for($i = 1; $i < 32; $i++){
        $i = ($i < 10) ? "0{$i}" : $i;
        $days[] = $i;
      }

      $months = array();
      for($i = 1; $i < 13; $i++){
        $i = ($i < 10) ? "0{$i}" : $i;
        $months[] = $i;
      }

      $years = array();
      for($i = date('Y',time()); $i > 1990; $i --){
          $years[] = $i;
      }

      $experiences = \src\Helpers\Experiences::returnAllInOrder();

      $this->params['days'] = $days;
      $this->params['months'] = $months;
      $this->params['years'] = $years;
      $this->params['experiences'] = $experiences;

      $this->template = 'admin/experiences.twig';
      $this->twigCalled = 1;
  	}	

  	// Non connecté
  	else{
  		$this->template = 'admin/connexion.twig';
      $this->twigCalled = 1;
  	}
  }
}
