<?php

namespace src\Controllers\Admin;

use \App\FrontController as FrontController;

use Symfony\Component\HttpFoundation as HttpFoundation;
/**
 * Class de construction de la page accueil admin
 * 
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Basic extends FrontController
{

  public function __construct( 
    array $params, 
    HttpFoundation\ServerBag $server, 
    HttpFoundation\FileBag $files, 
    HttpFoundation\ParameterBag $post, 
    HttpFoundation\ParameterBag $get, 
    HttpFoundation\ParameterBag $cookies
    )
  {
    
  	// Connecté
  	if( \src\Helpers\Site\Session::GetSession(SESSION_USER_NAME) !== 0 ){

      if( !isset($params['action']) ){
        $description = \src\Helpers\Description::returnAll();  
        $this->params['description'] = $description; 
      }

      $this->template = 'admin/accueil.twig';
      $this->twigCalled = 1;
  	}	

  	// Non connecté
  	else{
  		$this->template = 'admin/connexion.twig';
      $this->twigCalled = 1;
  	}
  }
}
