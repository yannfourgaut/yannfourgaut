<?php

namespace src\Controllers\Admin;

use \App\FrontController as FrontController;
use Symfony\Component\HttpFoundation as HttpFoundation;

/**
 * Class de connexion à l'admin
 * 
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Connexion extends FrontController
{

  public function __construct( 
    array $params, 
    HttpFoundation\ServerBag $server, 
    HttpFoundation\FileBag $files, 
    HttpFoundation\ParameterBag $post, 
    HttpFoundation\ParameterBag $get, 
    HttpFoundation\ParameterBag $cookies
    )
  {

  	$app = \src\Helpers\AppGlobal::$params;

    $pseudo = $post->get('pseudo');
    $pass = md5(trim($post->get('password')));

    $exists = $app['orm.em']->getRepository('\src\Entity\Users')->Exists($pseudo,$pass);
    if( $exists ){

      $exists = $exists[0];

      if($exists['exist'] == '1'){
        \src\Helpers\Site\Session::SetSession(SESSION_USER_NAME,'1');
      }
      else{

      }

    }
    
    $this->redirect = '/admin';
  }
}
