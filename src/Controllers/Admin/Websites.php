<?php 

namespace src\Controllers\Admin;

use Symfony\Component\HttpFoundation as HttpFoundation;
use \src\Helpers\AppGlobal as app;

class Websites extends \App\FrontController{

	public function __construct( 
	    array $params, 
	    HttpFoundation\ServerBag $server, 
	    HttpFoundation\FileBag $files, 
	    HttpFoundation\ParameterBag $post, 
	    HttpFoundation\ParameterBag $get, 
	    HttpFoundation\ParameterBag $cookies
    ){

		$app = app::$params;

		$this->params['websites'] = $app['orm.em']->getRepository('src\Entity\Sites')->findAll();

		$this->twigCalled = 1;
		$this->template = 'admin/websites.twig';


	}

}