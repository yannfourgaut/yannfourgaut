<?php

namespace src\Controllers\Admin;

use \App\FrontController as FrontController;
use Symfony\Component\HttpFoundation as HttpFoundation;

/**
 * Class d'enregistrement de posts
 * 
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Post extends FrontController
{

  public function __construct( 
    array $params, 
    HttpFoundation\ServerBag $server, 
    HttpFoundation\FileBag $files, 
    HttpFoundation\ParameterBag $post, 
    HttpFoundation\ParameterBag $get, 
    HttpFoundation\ParameterBag $cookies
    )
  {

  	// Connecté
  	if( \src\Helpers\Site\Session::GetSession(SESSION_USER_NAME) !== 0 ){

      if( $post->has('target') ){

        if($post->get('target') == 'description'){

          new \src\Helpers\Admin\UpdateDescription($post);

        } elseif($post->get('target') == 'website'){

          new \src\Helpers\Admin\Websites\Create($server, $post, $files);

        } elseif($post->get('target') == 'experiences'){

          $save = new \src\Helpers\Admin\Experiences\AddExperience($post);

          if($save->success === 1){

            $this->redirect = '/admin/action=experiences';
            return;

          } else{

            echo $save->success;
            die;

          }

        } elseif($post->get('target') == 'experienceUpdate'){

          $save = new \src\Helpers\Admin\Experiences\UpdateExperience($post);

          if($save->success === 1){

            $this->redirect = '/admin/action=experiences';
            return;

          } else{

            echo $save->success;
            die;

          }

        }

      }
  	}	

  	// Non connecté
  	else{
  		
  	}

    $this->redirect = '/admin';
  }
}
