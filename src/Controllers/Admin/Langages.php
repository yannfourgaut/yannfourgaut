<?php

namespace src\Controllers\Admin;

use \src\Helpers\AppGlobal as app;
use \App\FrontController as FrontController;
use Symfony\Component\HttpFoundation as HttpFoundation;

/**
 * Class admin de gestion des langages maitrisés
 * 
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Langages extends FrontController
{

  public $succes;

  public $langages;

  public function __construct( 
    array $params, 
    HttpFoundation\ServerBag $server, 
    HttpFoundation\FileBag $files, 
    HttpFoundation\ParameterBag $post, 
    HttpFoundation\ParameterBag $get, 
    HttpFoundation\ParameterBag $cookies
    )
  {
    
    $app = app::$params;

  	// Connecté
  	if( \src\Helpers\Site\Session::GetSession(SESSION_USER_NAME) !== 0 ){

      if( $post->get('target') !== null && $files->has('thumbnail') ){
          $name = $post->get('name');
          $file = $files->get('thumbnail');

          $link = $file->getPathName();
          $mimeType = $file->getMimeType();
          $extension = explode('/',$mimeType)[1];

          $img = new \Imagick($file->getPathName());
            $img->setImageFormat( $extension );
            $img->adaptiveResizeImage(200,200);

          $nameFile = str_replace(" ","_",$name);
          $nameFile = strtolower($nameFile);  

          $target = $_SERVER['DOCUMENT_ROOT']."/web/img/langages/{$nameFile}.{$extension}"; 
          $beautifulLink = str_replace($_SERVER['DOCUMENT_ROOT']."/web","",$target);

          if ($img->writeImage( $target )){

            $this->success = 'Sauvegarde effectuée';

            try{

                $entity = new \src\Entity\Langages;
                  $entity->setName($name);
                  $entity->setImg($beautifulLink);
                $app['orm.em']->persist( $entity );
                $app['orm.em']->flush(); 

            } catch(\Exception $e){

              $this->success = $e->getMessage();

            }

            $this->params['success'] = $this->success;

          }
      }

      $this->langages = \src\Helpers\Langages::returnAll();

      $this->params['langages'] = $this->langages;

      $this->template = 'admin/langages.twig';
      $this->twigCalled = 1;
  	}	

  	// Non connecté
  	else{
  		$this->template = 'admin/connexion.twig';
      $this->twigCalled = 1;
  	}
  }
}
