<?php

namespace src\Controllers\Admin;

use \App\FrontController as FrontController;
use Symfony\Component\HttpFoundation as HttpFoundation;

/**
 * Class de deconnexion admin
 * 
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Deconnexion extends FrontController
{

 public function __construct( 
    array $params, 
    HttpFoundation\ServerBag $server, 
    HttpFoundation\FileBag $files, 
    HttpFoundation\ParameterBag $post, 
    HttpFoundation\ParameterBag $get, 
    HttpFoundation\ParameterBag $cookies
    )
  {

    \src\Helpers\Site\Session::destroySession(SESSION_USER_NAME);
    $this->redirect = '/admin';
  }
}
