<?php

namespace src\Controllers\Contact;

use \App\FrontController as FrontController;
use Symfony\Component\HttpFoundation as HttpFoundation;

/**
 * Controleur d'envoi de mail
 *
 * Par convention, quand aucune action n'est appelée en query URL, c'est la class Basic qui est appelée
 * 
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Basic extends FrontController
{

  public function __construct( 
    array $params, 
    HttpFoundation\ServerBag $server, 
    HttpFoundation\FileBag $files, 
    HttpFoundation\ParameterBag $post, 
    HttpFoundation\ParameterBag $get, 
    HttpFoundation\ParameterBag $cookies
    )
  {

      if( 
        $post->get('name') && !empty($post->get('name') ) && 
        $post->get('message') && !empty($post->get('message')) && 
        $post->get('subject') && !empty($post->get('subject') ) && 
        $post->get('mail') && !empty($post->get('mail') )
      )
      {

        $message = $post->get('message');
        $subject = $post->get('subject');
        $name = $post->get('name');
        $mail = $post->get('mail');

        $message = (new \Swift_Message($subject))
        ->setFrom($mail, 'Mail du site envoyé par '.$name)
        ->setTo('contact@yannfourgaut.com')
        ->setBody($message, 'text/html');
        
        \src\Helpers\AppGlobal::$params['mailer']->send($message);

      }

      $this->template = 'home.twig';
      $this->twigCalled = 1;

      $description = \src\Helpers\Description::returnAll();  
      $packages = \src\Helpers\Packages::returnAll(); 
      $langages = \src\Helpers\Langages::returnAll();
      $experiences = \src\Helpers\Experiences::returnAllInOrder();

      $this->params['description'] = $description;    
      $this->params['packages'] = $packages; 
      $this->params['langages'] = $langages;
      $this->params['experiences'] = $experiences;
  }
}
