<?php 

namespace src\Controllers\Errors;

use \App\FrontController as FrontController;
use Symfony\Component\HttpFoundation as HttpFoundation;
/**
 * Class de gestion d'erreurs 
 *
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Basic extends FrontController
{
     /**
     *	Réponse en JSON | TWIG | redirection selon paramètres et réponse de requête
     *
     * @param array $params Les paramètres transmis en URL
     * @param object $post Les POST HTTP
     * @param object $get Les GET HTTP
     * @param object $cookies Les cookies
     */
    
    public function __construct( 
        array $params, 
        HttpFoundation\ServerBag $server, 
        HttpFoundation\FileBag $files, 
        HttpFoundation\ParameterBag $post, 
        HttpFoundation\ParameterBag $get, 
        HttpFoundation\ParameterBag $cookies
    )
    {   
        $this->twigCalled = 1;
        $this->template = 'messages/errors.twig';

        if (!isset($params['message'])) {
            $this->params['message'] = "Page inexistante";
        }
    }
}
