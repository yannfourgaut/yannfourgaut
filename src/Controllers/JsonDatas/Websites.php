<?php 

namespace src\Controllers\JsonDatas;

use \App\FrontController as FrontController;

class Websites extends \App\FrontController{

	public function __construct(){

		$return = array();
		$websites = \src\Helpers\Websites::returnAll(); 
		foreach($websites as $website){

			$id = $website->getId();
			$return[$id] = array();
				$return[$id]['name'] = $website->getName();
				$return[$id]['description'] = $website->getDescription();
				$return[$id]['link'] = $website->getLink();
				$return[$id]['imageColor'] = $website->getImageColor();
				$return[$id]['imageBlack'] = $website->getImageBlack();

		}

		$this->json = $return;

	}

}