<?php 

namespace src\Helpers;

class Experiences{

	static function returnAllInOrder(){

	$app = AppGlobal::$params;

	$allExperiences =  $app['orm.em']->getRepository('\src\Entity\Experiences')
        ->returnInOrder();

       foreach($allExperiences as $key => $experiences){

       		foreach($experiences as $key2 => $experience){

            if($key == 'actual'){
              $newDate = new \DateTime();
                $newDate->setDate(date('Y',time()), date('m',time()), date('d',time()));
                $newDate->setTime(0,0,0);
                $experience['date_end'] = $newDate;
            }
            
       			$interval = date_diff($experience['date_start'], $experience['date_end']);

       			if( $interval->format('%M') !== '00' ){

              $duree = intval($interval->format('%M'));
       				$format = "{$duree} mois";

       			} else{

       				$format = "{$interval->format('%d')} jours";

       			}

            $allExperiences[$key][$key2]['dates'] = array( 'debut' => array(), 'fin' => array() );

            $timestampDebut = strtotime($experience['date_start']->format('Y-m-d'));
            $allExperiences[$key][$key2]['dates']['debut']['jour'] = date( 'd', $timestampDebut);
            $allExperiences[$key][$key2]['dates']['debut']['mois'] = date( 'm', $timestampDebut);
            $allExperiences[$key][$key2]['dates']['debut']['annee'] = date( 'Y', $timestampDebut);

            $timestampFin = strtotime($experience['date_end']->format('Y-m-d'));
            $allExperiences[$key][$key2]['dates']['fin']['jour'] = date( 'd', $timestampFin);
            $allExperiences[$key][$key2]['dates']['fin']['mois'] = date( 'm', $timestampFin);
            $allExperiences[$key][$key2]['dates']['fin']['annee'] = date( 'Y', $timestampFin);

				    $allExperiences[$key][$key2]['duree'] = $format;
            $allExperiences[$key][$key2]['description'] = htmlspecialchars_decode($experience['description']);

       			$allExperiences[$key][$key2]['date_start'] = $experience['date_start']->format('d/m/Y');
       			$allExperiences[$key][$key2]['date_end'] = $experience['date_end']->format('d/m/Y');
       		}
       } 

    return $allExperiences;   
	}

}