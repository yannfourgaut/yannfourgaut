<?php 

namespace src\Helpers;

/**
 * Class de listing des namespaces présent dans le dossier src/Controllers
 *
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class RetrieveNamespaces{

	/**
	 * @var array $namespaces Le tableau contenant les namespaces
	 */
	public $namespaces = array( 'files' => array(), 'folders' => array() );

	/**
	 * @var string $namespaceBase Le namespace racine
	 */
	public $namespaceBase = 'src\\\\Controllers';

	/**
	 * @var string $currentNamespace La déclaration du namespace courant dans lequel le script passe
	 */
	public $currentNamespace; 

	public function __construct(){

		$dir = __DIR__;
		$this->currentNamespace = $this->namespaceBase;
		self::listFiles($dir,$this->namespaceBase);

	} 

	/**
	 * Fonction de listing récursif de fichiers
	 * 
	 * @param string $dir Le dossier à parcourir
	 * @param string $namespace Le namespace du dossier parent
	 */
	function listFiles($dir,$namespace){

		$dir = realpath($dir);

		if (is_dir($dir)) {

		    if ($dh = opendir($dir)) {
		        while ( ($file = readdir($dh) ) !== false) {

		        	// Sous dossier
		            if( is_dir($dir.'/'.$file) && $file !== '.' && $file !== '..'){
		            	$this->currentNamespace = $namespace.'\\\\'.$file;
		            	$this->namespaces['folders'][ count($this->namespaces['folders']) ] = $this->currentNamespace;
		            	self::listFiles($dir.'/'.$file,$this->currentNamespace);
		            }

		            elseif( is_file($dir.'/'.$file) && $file !== '.' && $file !== '..'){
		            	$file = str_replace('.php','',$file);
		            	$this->currentNamespace = $namespace.'\\\\'.$file;
		            	$this->namespaces['files'][ count($this->namespaces['files']) ] = $this->currentNamespace;
		            	$this->currentNamespace = $this->namespaceBase;
		            }

		        }
		        closedir($dh);
		    }
		}
	}

}