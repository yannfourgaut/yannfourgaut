<?php 

namespace src\Helpers;

class Description{


	static function returnAll(){

	$app = AppGlobal::$params;

	$personnality = $app['orm.em']->getRepository('\src\Entity\Description')
        ->findByType(\Doctrine\DBAL\Types\Type::getType('PresentationType')->values[0]);

        $description['personnality']['content'] = $personnality[0];
        $description['personnality']['icon'] = '<span class="glyphicon glyphicon-user"></span>';

        $road = $app['orm.em']->getRepository('\src\Entity\Description')
        ->findByType(\Doctrine\DBAL\Types\Type::getType('PresentationType')->values[1]);

        $description['road']['content'] = $road[0];
        $description['road']['icon'] = '<span class="glyphicon glyphicon-road"></span>';

        $talent = $app['orm.em']->getRepository('\src\Entity\Description')
        ->findByType(\Doctrine\DBAL\Types\Type::getType('PresentationType')->values[2]);

        $description['talent']['content'] = $talent[0];
        $description['talent']['icon'] = '<span class="glyphicon glyphicon-blackboard"></span>';

        $about = $app['orm.em']->getRepository('\src\Entity\Description')
        ->findByType(\Doctrine\DBAL\Types\Type::getType('PresentationType')->values[3]);

        $description['about']['content'] = $about[0];
        $description['about']['icon'] = '<span class="glyphicon glyphicon-question-sign"></span>';

        return $description;
	}

}