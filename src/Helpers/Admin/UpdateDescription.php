<?php 

namespace src\Helpers\Admin;

use \src\Helpers\AppGlobal as app;

class UpdateDescription{

	public function __construct(\Symfony\Component\HttpFoundation\ParameterBag $post){

	$personnality = $post->get('personnality');
	$road = $post->get('road');
	$talent = $post->get('talent');
	$about = $post->get('about');

	$app = app::$params;

	// Update de la description 'personnality'
        $personnalityEntity = new \src\Entity\Description;
        $descriptionID = $app['orm.em']->getRepository('\src\Entity\Description')->findIdByType('personnality')[0]['id'];

        $personnalityEntity->setId($descriptionID);
        $personnalityEntity->setType(\Doctrine\DBAL\Types\Type::getType('PresentationType')->values[0]);
        $personnalityEntity->setText($personnality);

        $app['orm.em']->merge($personnalityEntity);
		$app['orm.em']->flush();

		// Update de la description 'road'
        $roadEntity = new \src\Entity\Description;
        $roadID = $app['orm.em']->getRepository('\src\Entity\Description')->findIdByType('road')[0]['id'];

        $roadEntity->setId($roadID);
        $roadEntity->setType(\Doctrine\DBAL\Types\Type::getType('PresentationType')->values[1]);
        $roadEntity->setText($road);

        $app['orm.em']->merge($roadEntity);
		$app['orm.em']->flush();

		// Update de la description 'talent'
        $talentEntity = new \src\Entity\Description;
        $talentID = $app['orm.em']->getRepository('\src\Entity\Description')->findIdByType('talent')[0]['id'];

        $talentEntity->setId($talentID);
        $talentEntity->setType(\Doctrine\DBAL\Types\Type::getType('PresentationType')->values[2]);
        $talentEntity->setText($talent);

        $app['orm.em']->merge($talentEntity);
		$app['orm.em']->flush();

		// Update de la description 'about'
        $aboutEntity = new \src\Entity\Description;
        $aboutID = $app['orm.em']->getRepository('\src\Entity\Description')->findIdByType('about')[0]['id'];

        $aboutEntity->setId($aboutID);
        $aboutEntity->setType(\Doctrine\DBAL\Types\Type::getType('PresentationType')->values[3]);
        $aboutEntity->setText($about);

        $app['orm.em']->merge($aboutEntity);
		$app['orm.em']->flush();
	}

}