<?php 

namespace src\Helpers\Admin\Websites;

use \src\Helpers\AppGlobal as app;

/**
 * Enregistrement d'un site 
 */

class Create{

	public function __construct($server, $post, $files){

		$app = app::$params;

		$link = $post->get('websiteLink');
		$name = $post->get('websiteName');
		$description = $post->get('websiteDescription');
		$imageColor = '';
		$imageBlack = '';

		$siteName = explode("/",str_replace("http://","",$link))[0];

		if($files->has('imageColor')){

			if($files->get('imageColor')){
				$file = new \src\Helpers\Upload($files->get('imageColor'),__DIR__."/../../../../web/img/sites","{$siteName}");
				$imageColor = str_replace($server->get('DOCUMENT_ROOT')."web/",'/',$file->newLink);	

				// Génération de l'image noir et blanc
				
				$newImageBlack = new \Imagick($file->newLink);

				$infos = pathinfo($file->newLink);
					$extension = $infos['extension'];

				$nameBlack = $infos['filename']."-black";
				$target = "{$infos['dirname']}/{$nameBlack}.{$extension}";

				$newImageBlack->transformImageColorspace( \Imagick::COLORSPACE_GRAY );
				$newImageBlack->setFormat($extension);

				if($newImageBlack->writeImage( $target )){
					$imageBlack = str_replace($server->get('DOCUMENT_ROOT')."web/",'/',$target);	
				}


			}
			
		}

		$website = new \src\Entity\Sites;
			$website->setName($name);
			$website->setDescription($description);
			$website->setLink($link);
			$website->setImageColor($imageColor);
			$website->setImageBlack($imageBlack);
		$app['orm.em']->persist($website);	
		$app['orm.em']->flush();
	}

}