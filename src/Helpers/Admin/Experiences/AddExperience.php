<?php 

namespace src\Helpers\Admin\Experiences;

use \src\Helpers\AppGlobal as app;

class AddExperience{

        public $success;

        public function __construct(\Symfony\Component\HttpFoundation\ParameterBag $post)
        {
                $app = app::$params;

                $poste = trim($post->get('poste'));
                $enterprise = trim($post->get('enterprise'));
                $link = trim($post->get('link'));
                $description = trim(htmlspecialchars($post->get('description')));
                $actual = ($post->get('actual') !== null) ? 1 : 0;


                $dateDebut = new \Datetime();
                $dateDebut->setDate($post->get('yearStart'),$post->get('monthStart'),$post->get('dayStart'));
                $dateDebut->setTime(0,0,0);

                $dateFin = new \Datetime();
                $dateFin->setDate($post->get('yearEnd'),$post->get('monthEnd'),$post->get('dayEnd'));
                $dateFin->setTime(0,0,0);

                $entity = new \src\Entity\Experiences;
                        $entity->setPoste( $poste );
                        $entity->setEnterprise( $enterprise );
                        $entity->setDateStart( $dateDebut );
                        $entity->setDateEnd( $dateFin );
                        $entity->setLink( $link );
                        $entity->setDescription( $description );
                        $entity->setActual( $actual );

                try{
                        $app['orm.em']->persist($entity);
                        $app['orm.em']->flush();
                        $this->success = 1;

                } catch(\Exception $e){
                        $this->success = $e->getMessage();
                }
        }        

}