<?php 

namespace src\Helpers\Site;

use \Symfony\Component\HttpFoundation as HttpFoundation;
use \src\Classes\Database as Database;
use \src\Entity\Cookies as CookiesEntity;
use \src\Helpers\AppGlobal as app; 

use src\Helpers\Site\Session as SessionHelper;

/**
 * Class de centralisation de gestion des cookies
 *
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Cookies
{

    /**
     * Fonction de création de relation cookie => user en base
     * Cette fonction crée une entrée dans la table cookies avec l'id_user en foreign key, 
     * retourn l'ID du cookie, crée le cookie avec cet ID en valeur dans l'en tete http
     * 
     * @param \src\Entity\Users $user L'entity user concernée
     */
    public function setCookieUser(\src\Entity\Users $user)
    {
        $app = app::$params;

        $em = $app['orm.em'];
        $entity = new CookiesEntity;
        $entity->setFdUser($user->getIdUser());
        $em->persist($entity);
        $em->flush();
        $idCookie = $entity->getIdCookie();
        $dateTime = new \DateTime();
        $dateTime->modify("+1 year");
        $cookie = new HttpFoundation\Cookie(COOKIE_USER_NAME, $idCookie, $dateTime);

        return $cookie;
    }

    /**
     * Fonction de vérification d'éxistence de cookie
     * 
     * @param  \Symfony\Component\HttpFoundation\ParameterBag $cookies Les cookies de l'en tete HTTP
     * 
     * @return int | 0 La valeur du cookie ou 0 si cookie inexistant
     */
    
    public function getCookie( \Symfony\Component\HttpFoundation\ParameterBag $cookies, string $cookieName)
    {
        if ($cookies->get($cookieName) !== null) {
            return $cookies->get($cookieName);
        } else {
            return 0;
        }
    }

    public function FindUserByCookie($cookie){

        $app = app::$params;
        $conn = $app['orm.em']->getConnection();
        $queryBuilder = $conn->createQueryBuilder();

        // vendor/doctrine/dbal/lib/Doctrine/DBAL/Query/QueryBuilder.php
        
        /**
         * Définir ici la requete et le traitement
         */

    }

    /**
     * Suppression du cookie
     * @return object $response
     */
    public function deleteCookie(string $cookieName, $redirection = ROOT)
    {
        $response = new HttpFoundation\RedirectResponse($redirection);
        $response->headers->clearCookie($cookieName);

        return $response;
    }
}
