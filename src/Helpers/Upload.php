<?php 

namespace src\Helpers;

/**
 * Class d'upload de fichier basé sur Symfony\Component\HttpFoundation\File\UploadedFile
 *
 * Cette class doit être appelée de la manière suivante : 
 * src\Helpers\Upload($fileObject,$targetDirectory,$targetName);
 * 
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */
class Upload{

	/**
	 * @var string $originalName Le nom du fichier d'origine
	 */
	private $originalName;

	/**
	 * @var string $mimeType Le type mime du fichier d'origine
	 */
	private $mimeType;

	/**
	 * @var string $extension L'extension' du fichier d'origine
	 */
	private $extension;

	/**
	 * @var string $targetDirectory Le lien absolu du dossier de destination
	 */
	private $targetDirectory;

	/**
	 * @var string $targetName Le nom du nouveau fichier
	 */
	private $targetName;

	/**
	 * @var object $newFile L'objet Symfony\Component\HttpFoundation\File du nouveau fichier
	 */
	public $newFile;

	/**
	 * @var string $newLink Le lien absolu du nouveau fichier
	 */
	public $newLink;

	/**
	 * Fonction d'upload de fichier 
	 * 
	 * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file Le fichier uploadé
	 * @param string $targetDirectory Le lien absolu du dossier de destination
	 * @param string $targetName Le nom du nouveau fichier
	 */
	public function __construct(
		\Symfony\Component\HttpFoundation\File\UploadedFile $file, 
		$targetDirectory, 
		$targetName = null
		){

		$this->originalName = $file->getClientOriginalName();
		$this->mimeType = $file->getMimeType();
		$this->extension = $file->getClientOriginalExtension();
		$this->targetDirectory = realpath($targetDirectory);
		$this->targetName = ($targetName == null) ? $this->originalName : "{$targetName}.{$this->extension}";

		if($file->getError() == 0){
			if($file->isValid()){
				try{
					$this->newFile = $file->move($this->targetDirectory,$this->targetName);
					$this->newLink = "{$this->targetDirectory}/{$this->targetName}";
				}
				catch(\Exception $e){

				}
			}
		}

	}

}