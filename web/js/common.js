
jQuery(document).ready(
	function(){

		jQuery('.showForm').click(
			function(){
				var target = jQuery(this).data('target');
				var hidden = jQuery(target).hasClass('hidden');

				if(hidden == true){
					jQuery(target).removeClass('hidden');
					jQuery(this).removeClass('fa-plus');
					jQuery(this).addClass('fa-minus');
				}
				else{
					jQuery(target).addClass('hidden');
					jQuery(this).removeClass('fa-minus');
					jQuery(this).addClass('fa-plus');
				}
			}
		);

		$(document).on('scroll',function()
	    {
	        var top = $('html,body').scrollTop();
	        if(top>'50')
	        {
	          $('.to-top').css('display','block');
	        }
	        else
	        {
	          $('.to-top').css('display','none');
	        }
	    });

	    $(document).on('click','.to-top',function()
	    {
	        $('html,body').animate({'scrollTop':'0'},500);
	    });

	}
);