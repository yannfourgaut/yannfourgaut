
// declare a module
var AppGlobal = angular.module('AppGlobal', []);

AppGlobal.config(function($interpolateProvider){
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});