

AppGlobal.controller('HomeController', ['$scope','$http', '$compile',

	function($scope,$http,$compile) {

      $scope.captchaResult = 0;

  		$scope.changeBackgroundWebsite = function(img,e){
  			angular.element(e.target).attr('src',img);
  		}

  		$scope.loadWebsites = function(){
  			$http.get('/json-datas/action=websites').then(
  				function(response){
  					$scope.websites = response.data;
  				}
  			);
  		}	

  		$scope.buildModal = function(id){

  			var website = $scope.websites[id];

  			var html = '<div class="row">';

  					html += '<div class="col-sm-12">';
  						html += '<h2><a href="'+website.link+'" target="_blank">'+website.name+'</a></h2>';
  					html += '</div>';

  					html += '<div class="col-sm-6">';
  						html += '<img src="'+website.imageColor+'" class="img img-responsive"/>';
  					html += '</div>';
  					
  					html += '<div class="col-sm-6">';
  						html += website.description;
  					html += '</div>';	

  				html += '</div>';

  			$('.modal-body').html( ($compile)(html)($scope) );	
  			$('#myModal').modal({
			    show:true,
			    backdrop:true,
			    keyboard:true
			});

  		}

      $scope.controlCapatcha = function(){
        $scope.firstNumber = Math.floor( Math.random() * 10 );
        $scope.secondNumber = Math.floor( Math.random() * 10 );
      }

      $scope.isCaptchaOk = function(e){

          if( $scope.captchaResult !== ( $scope.firstNumber + $scope.secondNumber  ) ){
            alert('L\'addition est pourtant simple..');
            e.preventDefault();
            return false;
          }
      }

	}

]);